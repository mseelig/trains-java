package trains.graph;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class NodeTest {
    private Node nodeA;

    @BeforeEach
    void setUp() {
        nodeA = Node.getInstance("A");
    }

    @Test
    void should_ReturnEqualsForMultitonInstance() {
        Node nodeTester = Node.getInstance("A");
        assertEquals(nodeA, nodeTester);
    }

    @Test
    void should_ReturnEqualsForLowerCaseInstance() {
        Node nodeTester = Node.getInstance("a");
        assertEquals(nodeA, nodeTester);
    }

    @Test
    void testGetLabel() {
        assertEquals("A", nodeA.getLabel());
    }

    @Test
    void testToString() {
        assertEquals("A", nodeA.toString());
    }
}