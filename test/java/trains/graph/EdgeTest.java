package trains.graph;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EdgeTest {
    private Node nodeA;
    private Node nodeB;
    private Node nodeC;
    private Edge edgeAB2;

    @BeforeEach
    void setUp() {
        nodeA = Node.getInstance("A");
        nodeB = Node.getInstance("B");
        edgeAB2 = new Edge(nodeA, nodeB, 2);
    }

    @Test
    void should_CreateEdgeFromEdgeString() {
      Edge edgeTester = new Edge("ab2");
      assertAll(
              () -> assertEquals(nodeA, edgeTester.getFrom()),
              () -> assertEquals(nodeB, edgeTester.getTo()),
              () -> assertEquals(2, edgeTester.getDistance())
      );
    }

    @Test
    void testGetDistance() {
        assertEquals(2, edgeAB2.getDistance());
    }

    @Test
    void testGetFrom() {
        assertEquals(nodeA, edgeAB2.getFrom());
    }

    @Test
    void testGetTo() {
        assertEquals(nodeB, edgeAB2.getTo());
    }

    @Test
    void testToString() {
        assertEquals("A->B(2)", edgeAB2.toString());
    }
}