package trains.graph;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class AdjacencyListGraphTest {
    private Node nodeA;
    private Node nodeB;
    private Node nodeC;
    private Edge edgeAB2;
    private Edge edgeBC3;
    private Edge edgeAB5;
    private Edge edgeAC4;
    private Graph emptyGraph;

    @BeforeEach
    void setUp() {
        nodeA = Node.getInstance("A");
        nodeB = Node.getInstance("B");
        nodeC = Node.getInstance("C");
        edgeAB2 = new Edge(nodeA, nodeB, 2);
        edgeBC3 = new Edge(nodeB, nodeC, 3);
        edgeAB5 = new Edge(nodeA, nodeB, 5);
        edgeAC4 = new Edge(nodeA, nodeC, 4);
        emptyGraph = new AdjacencyListGraph();
    }

    @Test
    void testAddEdge() {
        Graph graph = new AdjacencyListGraph();
        graph.addEdge(edgeAB2);
        Collection<Node> expectedNodes = Set.of(nodeA, nodeB);
        Collection<Edge> expectedEdges = List.of(edgeAB2);

        assertAll(
                () -> assertEquals(expectedNodes, graph.getNodes()),
                () -> assertEquals(expectedEdges, graph.getEdges())
        );
    }

    @Test
    void testAddEdges() {
        Graph graph = new AdjacencyListGraph();
        graph.addEdges(List.of(edgeAB2, edgeAC4));
        Collection<Node> expectedNodes = Set.of(nodeA, nodeB, nodeC);
        Collection<Edge> expectedEdges = List.of(edgeAB2, edgeAC4);

        assertAll(
                () -> assertEquals(expectedNodes, graph.getNodes()),
                () -> assertEquals(expectedEdges, graph.getEdges())
        );
    }

    @Test
    void testAddEdgesWithEdgesString() {
        Graph graph = new AdjacencyListGraph();
        graph.addEdges("AB2, AC4");
        Collection<Node> expectedNodes = Set.of(nodeA, nodeB, nodeC);
        Collection<Edge> expectedEdges = List.of(edgeAB2, edgeAC4);

        assertAll(
                () -> assertEquals(expectedNodes, graph.getNodes()),
                () -> assertNotEquals(expectedEdges, graph.getEdges()) // new edges are generated when string is parsed
        );
    }

    @Test
    void testAddNode() {
        Graph graph = new AdjacencyListGraph();
        graph.addNode(nodeA);
        Collection<Node> expectedNodes = Set.of(nodeA);
        Collection<Edge> expectedEdges = List.of(); // graph has no edges

        assertAll(
                () -> assertEquals(expectedNodes, graph.getNodes()),
                () -> assertEquals(expectedEdges, graph.getEdges())
        );

    }

    @Test
    void testAddNodes() {
        Graph graph = new AdjacencyListGraph();
        graph.addNodes(Set.of(nodeA, nodeB, nodeC));
        Collection<Node> expectedNodes = Set.of(nodeA, nodeB, nodeC);
        Collection<Edge> expectedEdges = List.of(); // graph has no edges

        assertAll(
                () -> assertEquals(expectedNodes, graph.getNodes()),
                () -> assertEquals(expectedEdges, graph.getEdges())
        );
    }

    @Test
    void testFindEdge() {
        Graph graph = new AdjacencyListGraph();
        graph.addEdges(List.of(edgeAB2, edgeBC3));

        assertEquals(Optional.of(edgeAB2), graph.findEdge(nodeA, nodeB));
    }

    @Test
    void testFindEdgeWithEmptyGraph() {
        assertEquals(Optional.empty(), emptyGraph.findEdge(nodeA, nodeB));
    }

    @Test
    void testGetEdges() {
        Graph graph = new AdjacencyListGraph();
        graph.addEdges(List.of(edgeAB2, edgeAC4));
        Collection<Edge> expectedEdges = List.of(edgeAB2, edgeAC4);

        assertEquals(expectedEdges, graph.getEdges());
    }

    @Test
    void testGetNodes() {
        Graph graph = new AdjacencyListGraph();
        graph.addEdges(List.of(edgeAB2, edgeAC4));
        Collection<Node> expectedNodes = Set.of(nodeA, nodeB, nodeC);

        assertEquals(expectedNodes, graph.getNodes());
    }

    @Test
    void testHasNode() {
        Graph graph = new AdjacencyListGraph();
        graph.addEdges(List.of(edgeAB2));

        assertTrue(graph.hasNode(nodeB));
    }

    @Test
    void testHasNodeWhenMissing() {
        Graph graph = new AdjacencyListGraph();
        graph.addEdges(List.of(edgeAC4));

        assertFalse(graph.hasNode(nodeB));
    }

    @Test
    void testIsEmpty() {
        Graph graph = new AdjacencyListGraph();
        graph.addEdges(List.of(edgeAB2));

        assertAll (
                () -> assertTrue(emptyGraph.isEmpty()),
                () -> assertFalse(graph.isEmpty())
        );
    }

    @Test
    void testGetNeighboringEdges() {
        Graph graph = new AdjacencyListGraph();
        graph.addEdges(List.of(edgeAB2, edgeAC4, edgeAB5, edgeBC3));

        Collection<Edge> expectedNeighboringEdgesForNodeA = List.of(edgeAB2, edgeAC4, edgeAB5);
        Collection<Edge> expectedNeighboringEdgesForNodeB = List.of(edgeBC3);

        assertAll (
                () -> assertEquals(expectedNeighboringEdgesForNodeA, graph.getNeighboringEdges(nodeA)),
                () -> assertEquals(expectedNeighboringEdgesForNodeB, graph.getNeighboringEdges(nodeB))
        );
    }

    @Test
    void testGetNeighboringNodes() {
        Graph graph = new AdjacencyListGraph();
        graph.addEdges(List.of(edgeAB2, edgeAC4, edgeAB5, edgeBC3));

        Collection<Node> expectedNeighboringNodesForNodeA = Set.of(nodeB, nodeC);
        Collection<Node> expectedNeighboringNodesForNodeB = Set.of(nodeC);

        assertAll (
                () -> assertEquals(expectedNeighboringNodesForNodeA, graph.getNeighboringNodes(nodeA)),
                () -> assertEquals(expectedNeighboringNodesForNodeB, graph.getNeighboringNodes(nodeB))
        );
    }

    @Test
    void testToString() {
        Graph graph = new AdjacencyListGraph();
        graph.addEdges(List.of(edgeAB2, edgeAC4, edgeAB5, edgeBC3));

        assertEquals("[A->B(2), A->C(4), A->B(5), B->C(3)]", graph.toString());
    }

}