package trains.graph;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

class RouteTest {
    private Node nodeA;
    private Node nodeB;
    private Node nodeC;
    private Edge edgeAB1;
    private Edge edgeBC2;
    private Route routeABC3;

    @BeforeEach
    void setUp() {
        nodeA = Node.getInstance("A");
        nodeB = Node.getInstance("B");
        nodeC = Node.getInstance("C");
        edgeAB1 = new Edge(nodeA, nodeB, 1);
        edgeBC2 = new Edge(nodeB, nodeC, 2);
        routeABC3 = new Route(List.of(edgeAB1, edgeBC2));
    }

    @Test
    void testAddEdge() {
        Route routeTester = new Route();

        routeTester.addEdge(edgeAB1);
        assertAll(
                () -> assertEquals(1, routeTester.getEdges().size()),
                () -> assertEquals(OptionalInt.of(1), routeTester.getDistance())
        );
    }

    @Test
    void testCopy() {
        Route routeTester = routeABC3.copy();

        assertAll(
                () -> assertEquals(routeABC3.getEdges(), routeTester.getEdges()),
                () -> assertEquals(routeABC3.getDistance(), routeTester.getDistance()),
                () -> assertNotEquals(routeTester, routeABC3) // different objects
        );
    }

    @Test
    void testGetEdges() {
        Collection<Edge> edges = routeABC3.getEdges();
        Edge[] expectedEdges = { edgeAB1, edgeBC2 };

        assertArrayEquals(expectedEdges, edges.toArray());
    }

    @Test
    void testGetDest() {
        assertEquals(Optional.of(nodeC), routeABC3.getDest());
    }

    @Test
    void testGetDestWithEmptyRoute() {
        Route emptyRoute = new Route();
        assertEquals(Optional.empty(), emptyRoute.getDest());
    }

    @Test
    void testGetDistance() {
        assertEquals(OptionalInt.of(3), routeABC3.getDistance());
    }

    @Test
    void testGetDistanceWithEmptyRoute() {
        Route emptyRoute = new Route();
        assertEquals(OptionalInt.empty(), emptyRoute.getDistance());
    }

    @Test
    void testHasEdge() {
        Edge edgeAC3 = new Edge(nodeA, nodeC, 3);
        assertAll(
                () -> assertTrue(routeABC3.hasEdge(edgeAB1)),
                () -> assertTrue(routeABC3.hasEdge(edgeBC2)),
                () -> assertFalse(routeABC3.hasEdge(edgeAC3))
        );
    }

    @Test
    void testGetOrig() {
        assertEquals(Optional.of(nodeA), routeABC3.getOrig());
    }

    @Test
    void testGetOrigWithEmptyRoute() {
        Route emptyRoute = new Route();
        assertEquals(Optional.empty(), emptyRoute.getOrig());
    }

    @Test
    void testGetStops() {
        assertEquals(OptionalInt.of(2), routeABC3.getStops());
    }

    @Test
    void testGetStopsWithEmptyRoute() {
        Route emptyRoute = new Route();
        assertEquals(OptionalInt.empty(), emptyRoute.getStops());
    }

    @Test
    void testIsEmpty() {
        Route emptyRoute = new Route();
        assertAll(
                () -> assertTrue(emptyRoute.isEmpty()),
                () -> assertFalse(routeABC3.isEmpty())
        );
    }

    @Test
    void testMerge() {
        Edge edgeCD1 = new Edge("CD1");
        Edge edgeDA4 = new Edge("DA4");
        Route routeCDA5 = new Route(List.of(edgeCD1, edgeDA4));
        Route mergedRoute = routeABC3.merge(routeCDA5);
        Collection<Edge> expectedEdges = List.of(edgeAB1, edgeBC2, edgeCD1, edgeDA4);

        assertAll(
                () -> assertEquals(expectedEdges, mergedRoute.getEdges()),
                () -> assertEquals(OptionalInt.of(8), mergedRoute.getDistance())
        );
    }

    @Test
    void testPrependEdge() {
        Edge edgeCA2 = new Edge("CA2");

        assertEquals(OptionalInt.of(3), routeABC3.getDistance());

        routeABC3.prependEdge(edgeCA2);
        Collection<Edge> expectedEdges = List.of(edgeCA2, edgeAB1, edgeBC2);

        assertAll(
                () -> assertEquals(expectedEdges, routeABC3.getEdges()),
                () -> assertEquals(OptionalInt.of(5), routeABC3.getDistance())
        );
    }

    @Test
    void testRemoveLastEdge() {
        assertEquals(OptionalInt.of(3), routeABC3.getDistance());

        routeABC3.removeLastEdge();
        Collection<Edge> expectedEdges = List.of(edgeAB1);

        assertAll(
                () -> assertEquals(expectedEdges, routeABC3.getEdges()),
                () -> assertEquals(OptionalInt.of(1), routeABC3.getDistance())
        );
    }

    @Test
    void testNumberOfStops() {
        assertEquals(OptionalInt.of(2), routeABC3.getNumberOfStops());
    }

    @Test
    void testNumStopsWithEmptyRoute() {
        Route emptyRoute = new Route();
        assertEquals(OptionalInt.empty(), emptyRoute.getNumberOfStops());
    }

    @Test
    void testToString() {
        assertEquals("Route(3): A->B->C", routeABC3.toString());
    }

    @Test
    void testToStringWithEmptyRoute() {
        Route emptyRoute = new Route();

        assertEquals("Empty Route", emptyRoute.toString());
    }
}