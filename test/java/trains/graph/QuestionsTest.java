package trains.graph;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import trains.graph.algorithms.DFS;
import trains.graph.algorithms.GraphTraversalAlgorithm;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.OptionalInt;

import static org.junit.jupiter.api.Assertions.*;

public class QuestionsTest {
    final String EDGES_STRING = "AB5, BC4, CD8, DC8, DE6, AD5, CE2, EB3, AE7";
    private Node nodeA;
    private Node nodeB;
    private Node nodeC;
    private Node nodeD;
    private Node nodeE;
    private Graph graph;
    private TripsGenerator tripsGenerator;

    @BeforeEach
    void setUp() {
        nodeA = Node.getInstance("A");
        nodeB = Node.getInstance("B");
        nodeC = Node.getInstance("C");
        nodeD = Node.getInstance("D");
        nodeE = Node.getInstance("E");
        graph = new AdjacencyListGraph();
        graph.addEdges(EDGES_STRING);
        GraphTraversalAlgorithm algorithm = new DFS(graph);
        tripsGenerator = new TripsGenerator(algorithm); // DI
    }

    @Test
    void testQuestion1() {
        List<Node> nodePath = List.of(nodeA, nodeB, nodeC);
        assertEquals(OptionalInt.of(9), GraphHelper.getRouteDistance(nodePath, graph));
    }

    @Test
    void testQuestion2() {
        List<Node> nodePath = List.of(nodeA, nodeD);
        assertEquals(OptionalInt.of(5), GraphHelper.getRouteDistance(nodePath, graph));
    }

    @Test
    void testQuestion3() {
        List<Node> nodePath = List.of(nodeA, nodeD, nodeC);
        assertEquals(OptionalInt.of(13), GraphHelper.getRouteDistance(nodePath, graph));
    }

    @Test
    void testQuestion4() {
        List<Node> nodePath = List.of(nodeA, nodeE, nodeB, nodeC, nodeD);
        assertEquals(OptionalInt.of(22), GraphHelper.getRouteDistance(nodePath, graph));
    }

    @Test
    void testQuestion5() {
        List<Node> nodePath = List.of(nodeA, nodeE, nodeD);
        assertEquals(OptionalInt.empty(), GraphHelper.getRouteDistance(nodePath, graph));
    }

    @Test
    void testQuestion6() {
        Collection<Route> foundTrips = tripsGenerator.findTripsBy(nodeC, nodeC, 3,
                TripsGenerator.ByMethod.STOPS, TripsGenerator.ByOperator.LEQ);
        assertEquals(2, foundTrips.size());
    }

    @Test
    void testQuestion7() {
        Collection<Route> foundTrips = tripsGenerator.findTripsBy(nodeA, nodeC, 4,
                TripsGenerator.ByMethod.STOPS, TripsGenerator.ByOperator.EQ, true);
        assertEquals(3, foundTrips.size());
    }

    @Test
    void testQuestion8() {
        Optional<Route> shortestTrip = tripsGenerator.findTripByLeastMethod(nodeA, nodeC,
                TripsGenerator.ByMethod.DISTANCE);
        assertEquals(OptionalInt.of(9), shortestTrip.map(Route::getDistance).get());
    }

    @Test
    void testQuestion9() {
        Optional<Route> shortestTrip = tripsGenerator.findTripByLeastMethod(nodeB, nodeB,
                TripsGenerator.ByMethod.DISTANCE);
        assertEquals(OptionalInt.of(9), shortestTrip.map(Route::getDistance).get());
    }

    @Test
    void testQuestion10() {
        Collection<Route> foundTrips = tripsGenerator.findTripsBy(nodeC, nodeC, 30,
                TripsGenerator.ByMethod.DISTANCE, TripsGenerator.ByOperator.LT, true);
        assertEquals(7, foundTrips.size());
    }

}
