package trains.graph;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public abstract class Graph {
   public abstract void addEdge(Edge e);

   public void addEdges(Collection<Edge> edges) {
      edges.forEach(this::addEdge);
   }

   public void addEdges(String edgesString) {
      Collection<Edge> edges = Graph.parseEdgesString(edgesString);
      edges.forEach(this::addEdge);
   }

   public abstract void addNode(Node n);

   public void addNodes(Collection<Node> nodes) {
      nodes.forEach(this::addNode);
   }

   public abstract Collection<Edge> getEdges();

   public Optional<Edge> findEdge(Node orig, Node dest) {
      if (this.isEmpty()) {
         return Optional.empty();
      }

      return getNeighboringEdges(orig).stream()
              .filter(e -> e.getTo().equals(dest))
              .findFirst();
   }

   public boolean hasNode(Node n) {
      return getNodes().contains(n);
   }

   public boolean isEmpty() {
      return getNodes().isEmpty();
   }

   public abstract Collection<Edge> getNeighboringEdges(Node n);

   public abstract Collection<Node> getNeighboringNodes(Node n);

   public abstract Collection<Node> getNodes();

   public String toString() {
      return getEdges().stream()
              .map(Edge::toString)
              .collect(Collectors.toList())
              .toString();
   }

   private static Collection<Edge> parseEdgesString(String edgesString) {
      List<String> edgesStringList = Arrays.asList(edgesString.split(","));
      return edgesStringList.stream()
              .map(String::strip)
              .map(Edge::new)
              .collect(Collectors.toList());
   }

}
