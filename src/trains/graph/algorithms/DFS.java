package trains.graph.algorithms;

import trains.graph.*;

import java.util.*;
import java.util.Stack;

public class DFS implements GraphTraversalAlgorithm {
    private final Graph graph;

    public DFS(Graph g) {
        this.graph = g;
    }

    public Collection<Route> findRoutes(Node orig, Node dest) {
        List<Route> collectedRoutes = new ArrayList<>();

        if (orig.equals(dest)) {
            graph.getNeighboringEdges(orig).forEach((e) -> {
                List<Route> neighboringRoutes = new ArrayList<>();
                Set<Node> visitedNodes = new HashSet<>();
                Stack<Node> nodePath = new Stack<>();
                findRoutesHelper(e.getTo(), dest, visitedNodes, nodePath, neighboringRoutes);
                neighboringRoutes.forEach((r) -> r.prependEdge(e));
                collectedRoutes.addAll(neighboringRoutes);
            });
        } else {
            Set<Node> visitedNodes = new HashSet<>();
            Stack<Node> nodePath = new Stack<>();
            findRoutesHelper(orig, dest, visitedNodes, nodePath, collectedRoutes);
        }
        return collectedRoutes;
    }

    private void findRoutesHelper(Node o, Node d, Set<Node> visited, Stack<Node> path, List<Route> routes) {
        visited.add(o);
        path.push(o);
        if (o.equals(d)) {
            if (path.size() > 1) {
                Optional<Route> r = GraphHelper.buildRoute(path, graph);
                r.ifPresent(routes::add);
            }
        } else {
            graph.getNeighboringNodes(o).forEach((n) -> {
                if (!visited.contains(n)) {
                    findRoutesHelper(n, d, visited, path, routes);
                }
            });
        }
        path.pop();
        visited.remove(o);
    }
}
