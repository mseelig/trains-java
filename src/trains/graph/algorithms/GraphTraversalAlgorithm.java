package trains.graph.algorithms;

import trains.graph.Node;
import trains.graph.Route;

import java.util.Collection;

public interface GraphTraversalAlgorithm {
    Collection<Route> findRoutes(Node orig, Node dest);
}
