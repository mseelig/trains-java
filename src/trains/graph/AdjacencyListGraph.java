package trains.graph;

import java.util.*;
import java.util.stream.Collectors;

public class AdjacencyListGraph extends Graph {
    private HashMap<String, List<Edge>> adjacencyHash = new HashMap<>();
    private Collection<Node> nodes = new HashSet<>();

    public void addEdge(Edge e) {
        addNode(e.getFrom());
        addNode(e.getTo());
        addNeighboringEdge(e);
    }

    public void addNode(Node n) {
        if (!nodes.contains(n)) {
            nodes.add(n);
        }
    }

    public Collection<Edge> getEdges() {
        return adjacencyHash.values().stream()
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
    }

    public Collection<Edge> getNeighboringEdges(Node n) {
        return adjacencyHash.get(n.getLabel());
    }

    public Collection<Node> getNeighboringNodes(Node n) {
        return adjacencyHash.get(n.getLabel()).stream()
                .map(Edge::getTo)
                .collect(Collectors.toSet());
    }

    public Collection<Node> getNodes() {
        return nodes;
    }

    private void addNeighboringEdge(Edge e) {
        // TODO: raise DuplicateEdgeError if neighboring edge exists
        adjacencyHash.computeIfAbsent(e.getFrom().getLabel(), k -> new ArrayList<>()).add(e);
    }
}
