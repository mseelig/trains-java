package trains.graph;

import java.util.Comparator;

public class RouteDistanceComparator implements Comparator<Route> {

    @Override
    public int compare(Route firstRoute, Route secondRoute) {
        return Integer.compare(firstRoute.getDistance().getAsInt(), secondRoute.getDistance().getAsInt());
    }
}
