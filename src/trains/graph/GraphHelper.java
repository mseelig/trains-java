package trains.graph;

import java.util.List;
import java.util.Optional;
import java.util.OptionalInt;

public class GraphHelper {
    public static Optional<Route> buildRoute(List<Node> nodes, Graph graph) {
        Route route = new Route();
        int i = 0;
        while(i < nodes.size()-1) {
           Optional<Edge> edge = graph.findEdge(nodes.get(i), nodes.get(i+1));
           if (edge.isEmpty()) {
               return Optional.empty();
           } else {
               route.addEdge(edge.get());
           }
           i++;
        }

        if (route.isEmpty()) {
            return Optional.empty();
        } else {
            return Optional.of(route);
        }
    }

    public static OptionalInt getRouteDistance(List<Node> nodes, Graph graph) {
        Optional<Route> route = buildRoute(nodes, graph);
        if (route.isPresent()) {
            return route.get().getDistance();
        } else {
            return OptionalInt.empty();
        }
    }

    public static String printRouteDistance(List<Node> nodes, Graph graph) {
        OptionalInt dist = getRouteDistance(nodes, graph);
        if (dist.isPresent()) {
            return String.valueOf(dist.getAsInt());
        } else {
            return "NO SUCH ROUTE";
        }
    }
}
