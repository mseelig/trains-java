package trains.graph;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

// multiton class
public class Node {
    private static final ConcurrentMap<String, Node> nodes = new ConcurrentHashMap<>();

    private final String label;

    private Node(String label) {
        this.label = label;
    }

    public static Node getInstance(final String label) {
        return nodes.computeIfAbsent(label.toUpperCase(), Node::new);
    }

    public String getLabel() {
       return label;
    }

    public String toString() {
        return label;
    }
}
