package trains.graph;

public class Edge {
    private final Node from, to;
    private final int distance;

    public Edge(String edgeString) {
        this.from = Node.getInstance(edgeString.substring(0,1));
        this.to = Node.getInstance(edgeString.substring(1,2));
        this.distance = Integer.valueOf(edgeString.substring(2,3));
    }

    public Edge(Node from, Node to, int distance) {
        this.from = from;
        this.to = to;
        this.distance = distance;
    }

    public int getDistance() {
        return distance;
    }

    public Node getFrom() {
        return from;
    }

    public Node getTo() {
        return to;
    }

    public String toString() {
        return from + "->" + this.to + "(" + this.distance + ")";
    }
}
