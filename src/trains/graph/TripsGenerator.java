package trains.graph;

import trains.graph.algorithms.GraphTraversalAlgorithm;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

// TripsGenerator finds a collection of traversable routes between two nodes of a graph
public class TripsGenerator {
    private final GraphTraversalAlgorithm algorithm; // uses DI

    public enum ByMethod {
        STOPS,
        DISTANCE
    }

    public enum ByOperator {
        LT {
            @Override
            public String getOperatorMethodName() {
                return "isLessThan";
            }
        },
        EQ {
            @Override
            public String getOperatorMethodName() {
                return "isEqualTo";
            }
        },
        LEQ {
            @Override
            public String getOperatorMethodName() {
                return "isLessThanOrEqualTo";
            }
        };

        public abstract String getOperatorMethodName();
    }

    public TripsGenerator(GraphTraversalAlgorithm algorithm) {
        this.algorithm = algorithm;
    }

    public Collection<Route> findTripsBy(Node orig, Node dest, int val,
                                         ByMethod methodEnum,
                                         ByOperator operatorEnum) {
        return findTripsBy(orig, dest, val, methodEnum, operatorEnum, false);
    }

    public Collection<Route> findTripsBy(Node orig, Node dest, int val,
                                         ByMethod methodEnum,
                                         ByOperator operatorEnum,
                                         Boolean includeCycles) {
        Collection<Route> allTripsWithoutCycles = findAllTrips(orig, dest);

        if (!includeCycles) {
            return filterTripsBy(allTripsWithoutCycles, methodEnum, operatorEnum, val);
        }

        Collection<Route> allCycleTrips = findAllTrips(dest, dest);
        Collection<Route> collectedTripsWithCycles = new ArrayList<>();
        // optimize by filtering allTripsWithoutCycles, allCycleTrips by LEQ
        Collection<Route> filteredTripsWithoutCycles = filterTripsBy(allTripsWithoutCycles, methodEnum,
                ByOperator.LEQ, val);
        Collection<Route> filteredCycleTrips = filterTripsBy(allCycleTrips, methodEnum,
                ByOperator.LEQ, val);
        filteredTripsWithoutCycles.forEach((trip) ->
                findTripsWithCycles(trip, val, methodEnum, filteredCycleTrips, collectedTripsWithCycles));

        Collection<Route> collectedTrips = Stream.of(filteredTripsWithoutCycles, collectedTripsWithCycles)
                .flatMap(Collection::stream)
                .distinct()
                .collect(Collectors.toList());

        return filterTripsBy(collectedTrips, methodEnum, operatorEnum, val);
    }

    public Optional<Route> findTripByLeastMethod(Node orig, Node dest, ByMethod methodEnum) {
        Comparator<Route> routeComparator = (methodEnum == ByMethod.DISTANCE)
                ? new RouteDistanceComparator()
                : new RouteStopsComparator();
        return findAllTrips(orig, dest).stream().min(routeComparator);
    }

    private void findTripsWithCycles(Route trip, int val, ByMethod methodEnum,
                                     Collection<Route> cycleTrips, Collection<Route> collectedTrips) {
        Collection<Route> filteredTrips = filterTripsBy(cycleTrips, methodEnum, ByOperator.LEQ, val);
        if (filteredTrips.isEmpty()) {
            return;
        }

        filteredTrips.forEach((ft) -> {
            collectedTrips.add(trip);
            Route mergedTrip = trip.merge(ft);
            collectedTrips.add(mergedTrip);
            int ftVal = methodEnum.equals(ByMethod.DISTANCE) ? ft.getDistance().getAsInt() : ft.getStops().getAsInt();
            findTripsWithCycles(mergedTrip, val-ftVal, methodEnum, cycleTrips, collectedTrips);
        });
    }


    private Collection<Route> filterTripsBy(Collection<Route> trips,
                                            ByMethod methodEnum, ByOperator operatorEnum, int val) {
        Collection<Route> filteredTrips = new ArrayList<>();
        try {
            Method byMethod = Route.class.getDeclaredMethod((methodEnum == ByMethod.DISTANCE) ? "getDistance"
                    : "getStops");
            String operatorMethodName = operatorEnum.getOperatorMethodName();
            var operatorArgTypes = new Class[]{int.class, int.class};
            Method byOperator = getClass().getDeclaredMethod(operatorMethodName, operatorArgTypes);
            filteredTrips = trips.stream()
                    .filter(r -> {
                        try {
                            OptionalInt distanceOrStops = (OptionalInt) byMethod.invoke(r);
                            return (boolean) byOperator.invoke(null, distanceOrStops.getAsInt(), val);
                        } catch (IllegalAccessException | InvocationTargetException ex) {
                            ex.printStackTrace();
                        }
                        return false;
                    })
                    .collect(Collectors.toList());
        } catch (NoSuchMethodException ex) {
            ex.printStackTrace();
        }
        return filteredTrips;
    }

    private Collection<Route> findAllTrips(Node orig, Node dest) {
        return algorithm.findRoutes(orig, dest);
    }

    private static Boolean isLessThan(int a, int b) {
        return (a < b);
    }

    private static Boolean isLessThanOrEqualTo(int a, int b) {
        return (a <= b);
    }

    private static Boolean isEqualTo(int a, int b) {
        return (a == b);
    }
}

