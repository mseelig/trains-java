package trains.graph;

import java.util.Comparator;

public class RouteStopsComparator implements Comparator<Route> {

    @Override
    public int compare(Route firstRoute, Route secondRoute) {
        return Integer.compare(firstRoute.getStops().getAsInt(), secondRoute.getStops().getAsInt());
    }
}
