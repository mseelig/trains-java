package trains.graph;

import java.util.*;
import java.util.stream.Stream;

public class Route {
    private LinkedList<Edge> edgesList = new LinkedList<>();

    public Route() {}

    public Route(Collection<Edge> edges) {
        edgesList.addAll(edges);
    }

    public void addEdge(Edge e) {
        edgesList.add(e);
    }

    public Route copy() {
        Route newRoute = new Route();
        edgesList.forEach( (e) -> {
            newRoute.addEdge(e);
        });

        return newRoute;
    }

    public Collection<Edge> getEdges() {
        return edgesList;
    }

    public Optional<Node> getDest() {
        if (edgesList.isEmpty()) {
            return Optional.empty();
        } else {
            return Optional.of(edgesList.getLast().getTo());
        }
    }

    public OptionalInt getDistance() {
        if (edgesList.isEmpty()) {
            return OptionalInt.empty();
        }
        return OptionalInt.of(edgesList.stream()
                .map(Edge::getDistance)
                .reduce(0, Integer::sum));
    }

    public Boolean hasEdge(Edge edge) {
        return !edgesList.stream()
                .filter(e -> e.equals(edge))
                .findFirst()
                .isEmpty();
    }

    public Optional<Node> getOrig() {
        if (edgesList.isEmpty()) {
            return Optional.empty();
        } else {
            return Optional.of(edgesList.get(0).getFrom());
        }
    }

    public OptionalInt getStops() {
        if (edgesList.isEmpty()) {
            return OptionalInt.empty();
        }
        return OptionalInt.of(edgesList.size());
    }

    public Boolean isEmpty() {
        return edgesList.isEmpty();
    }

    public Route merge(Route route) {
        Route mergedRoute = new Route();
        Stream.of(this.getEdges(), route.getEdges())
                .flatMap(Collection::stream)
                .forEach(mergedRoute::addEdge);

        return mergedRoute;
    }

    public void prependEdge(Edge e) {
        edgesList.add(0, e);
    }

    public void removeLastEdge() {
        if (!edgesList.isEmpty()) {
            edgesList.remove(edgesList.size()-1);
        }
    }

    public OptionalInt getNumberOfStops() {
        if (isEmpty()) {
            return OptionalInt.empty();
        } else {
            return OptionalInt.of(edgesList.size());
        }
    }

    public String toString() {
        if (!this.isEmpty()) {
            String retVal = "Route("
                    + this.getDistance().getAsInt() + "): "
                    + this.getOrig().get().getLabel();
            for (Edge e : edgesList) {
                retVal += "->" + e.getTo().getLabel();
            }
            return retVal;
        } else {
            return "Empty Route";
        }
    }
}
