package trains;

import trains.graph.*;
import trains.graph.algorithms.DFS;
import trains.graph.algorithms.GraphTraversalAlgorithm;

import java.util.List;

public class Main {

    public static void main(String[] args) {
        final String EDGES_STRING = "AB5, BC4, CD8, DC8, DE6, AD5, CE2, EB3, AE7";

        Node a = Node.getInstance("a");
        Node b = Node.getInstance("b");
        Node b2 = Node.getInstance("b");
        Node c = Node.getInstance("c");
        Node d = Node.getInstance("d");
        Node e = Node.getInstance("e");

        Edge ab1 = new Edge("ab1");
        Edge bc2 = new Edge(b, c, 2);

        Graph g = new AdjacencyListGraph();
        g.addEdges(EDGES_STRING);

        List<Node> nodeList1 = List.of(a, b, c);
        System.out.println("Output #1: " + GraphHelper.printRouteDistance(nodeList1, g));
        List<Node> nodeList2 = List.of(a, d);
        System.out.println("Output #2: " + GraphHelper.printRouteDistance(nodeList2, g));
        List<Node> nodeList3 = List.of(a, d, c);
        System.out.println("Output #3: " + GraphHelper.printRouteDistance(nodeList3, g));
        List<Node> nodeList4 = List.of(a, e, b, c, d);
        System.out.println("Output #4: " + GraphHelper.printRouteDistance(nodeList4, g));
        List<Node> nodeList5 = List.of(a, e, d);
        System.out.println("Output #5: " + GraphHelper.printRouteDistance(nodeList5, g));

        GraphTraversalAlgorithm algorithm = new DFS(g);
        TripsGenerator tg = new TripsGenerator(algorithm); // DI

        System.out.println("Output #6: " + tg.findTripsBy(c, c, 3,
                TripsGenerator.ByMethod.STOPS, TripsGenerator.ByOperator.LEQ).size());
        System.out.println("Output #7: " + tg.findTripsBy(a, c, 4,
                TripsGenerator.ByMethod.STOPS, TripsGenerator.ByOperator.EQ, true)
                .size());
        System.out.println("Output #8: " + tg.findTripByLeastMethod(a, c,
                TripsGenerator.ByMethod.DISTANCE).get().getDistance().getAsInt());
        System.out.println("Output #9: " + tg.findTripByLeastMethod(b, b,
                TripsGenerator.ByMethod.DISTANCE).get().getDistance().getAsInt());
        System.out.println("Output #10: " + tg.findTripsBy(c, c, 30,
                        TripsGenerator.ByMethod.DISTANCE, TripsGenerator.ByOperator.LT,
                       true).size());
    }
}
